import config from './config'

import gulp from 'gulp';

require('require-dir')('./gulp/');

const watchTasks = gulp.parallel('watch:fonts','watch:images','watch:templates','watch:stylus', 'watch:css', 'watch:js:vendor');

gulp.task('watch', (cb) => {
	if(!config.isDevelopment) {
		cb();
	} else {
		watchTasks();
	}
});

gulp.task('default', gulp.series(
	// 'clean',
	gulp.parallel(
		'webpack',
		'js:vendor',
		'templates',
		'css',
		'stylus',
		'images',
		'fonts'
	),
	gulp.parallel(
		'serve',
		'watch'
	)
));

