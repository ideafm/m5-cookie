import path from 'path';
import webpack from 'webpack';

let srcBase = 'src';
let destBase = 'public';

const basePath = path.join(__dirname);

const PATH = {
	src: `${srcBase}`,
	dest: `${destBase}`,
	webpack: {
		loaders: {
			include: path.join(__dirname, srcBase)
		},
		output: {
			publicPath: '/js/'
		}
	},
	js: {
		src: `${srcBase}/js/*.js`,
		dest: `${destBase}/js`,
		vendorSrc: `${srcBase}/js/vendor/*.js`,
		vendorDest: `${destBase}/js/vendor`,
		webpack: {
			loaders: {
				include: path.join(__dirname, srcBase)
			}
		}
	},
	pug: {
		src: `${srcBase}/templates/*.pug`,
		dest: `${destBase}`,
		baseDir: `${srcBase}/templates`,
		manifestName: 'jadeManifest.json',
		watch: `${srcBase}/templates/**/*.pug`,
	},
	images: {
		src: `${srcBase}/img/**/*`,
		srcAll: `${srcBase}/img/**/*.{jpg,png,jpeg}`,
		srcSvg: `${srcBase}/img/**/*.svg`,
		dest: `${destBase}/img`
	},
	fonts: {
		src: `${srcBase}/fonts/**/*`,
		dest: `${destBase}/fonts/`
	},
	stylus: {
		src: `${srcBase}/stylus/styles.styl`,
		dest: `${destBase}/css/`
	},
	clean: `${destBase}/**`,
	watch: {
		jade: {
			includes: [`${srcBase}/jade/**/*.jade`, `!${srcBase}/jade/*.jade`]
		},
		stylus: `${srcBase}/stylus/**/*.styl`
	},
	css: {
		src: `${srcBase}/css/*.css`,
		dest: `${destBase}/css`
	}
};

const autoplefixerOptions = {
	browsers: ['last 2 versions', 'ie >= 9']
};

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'dev';

const webpackConfig = {
	output: {
		publicPath: PATH.webpack.output.publicPath,
		filename: isDevelopment ? "[name].js" : "[name].min.js",
		chunkFilename: isDevelopment ? "chunks/[name].[id].js" : "chunks/[name].[id].min.js"
	},
	watch: isDevelopment,
	devtool: isDevelopment ? 'cheap-module-inline-source-map' : false,
	module: {
		rules: [{
			test: /\.js$/,
			include: PATH.webpack.loaders.include,
			loader: 'babel-loader?presets[]=es2015'
		}]
	},
	plugins: isDevelopment ? [
		new webpack.NoEmitOnErrorsPlugin(),
		// new webpack.ProvidePlugin({
		// 	$: "jquery",
		// 	jQuery: "jquery",
		// 	"window.jQuery": "jquery",
		// 	'Promise': 'es6-promise'
		// }),
		new webpack.DefinePlugin({
			debug: JSON.stringify(isDevelopment),
			"process.env": {
				NODE_ENV: JSON.stringify(isDevelopment)
			}
		})
	] : [
		new webpack.NoEmitOnErrorsPlugin(),
		// new webpack.ProvidePlugin({
		// 	$: "jquery",
		// 	jQuery: "jquery",
		// 	"window.jQuery": "jquery",
		// 	'Promise': 'es6-promise'
		// }),
		new webpack.DefinePlugin({
			debug: JSON.stringify(isDevelopment),
			"process.env": {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV)
			}
		}),
		new webpack.optimize.UglifyJsPlugin({
			compress:{
				warnings: true
			}
		})
	]
};

const browserSync = {
	oprions: {
		server: {
			baseDir: `./${destBase}`
		}
	},
	watch: `${destBase}/**/*.*`
};


export default {
	basePath,
	PATH,
	webpackConfig,
	isDevelopment,
	browserSync,
	autoplefixerOptions
}