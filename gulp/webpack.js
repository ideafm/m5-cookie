import config from '../config'

import gulp from 'gulp';
import webpackStream from 'webpack-stream';
import named from 'vinyl-named';
import webpack2 from 'webpack';

// let webpack = webpackStream.webpack;

let g = require('gulp-load-plugins')();

gulp.task('webpack', (cb) => {
	let firstBuildReady = false;

	function done(err, stats) {
		firstBuildReady = true;

		if(err) {
			return;
		}

		g.util.log(stats.hasErrors() ? 'error': 'info', stats.toString({
			colors: g.util.colors.supportsColor
		}));
	}

	return gulp.src(config.PATH.js.src)
		.pipe(g.plumber())
		.pipe(named())
		.pipe(webpackStream(config.webpackConfig, webpack2, done))
		.pipe(gulp.dest(config.PATH.js.dest))
		.on('data', () => {
			if(firstBuildReady) {
				cb();
			}
	});
});