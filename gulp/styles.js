import config from '../config';

import gulp from 'gulp';
let g = require('gulp-load-plugins')();
console.log(config.isDevelopment);
gulp.task('stylus', ()=> {
	return gulp.src(config.PATH.stylus.src)
		.pipe(g.if(config.isDevelopment, g.sourcemaps.init()))
		.pipe(g.plumber())
		.pipe(g.stylus())
		.pipe(g.autoprefixer(config.autoplefixerOptions))
		.pipe(g.if(!config.isDevelopment, g.cssmin()))
		.pipe(g.if(!config.isDevelopment, g.rename({suffix: '.min'})))
		.pipe(g.if(config.isDevelopment, g.sourcemaps.write()))
		.pipe(gulp.dest(config.PATH.stylus.dest))
});

gulp.task('watch:stylus', () => {
	gulp.watch(config.PATH.watch.stylus, gulp.parallel('stylus'));
});