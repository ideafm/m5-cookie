;(function() {
    const Cookies = require('js-cookie');

    // data-cookie-text, data-cookie-allow-btn, data-cookie-more-link, data-cookie-more-btn, data-cookie-exclude, data-cookie-style-prefix

    let $body = document.body;
    let prefix = $body.getAttribute('data-cookie-style-prefix') || 'm5';

    let cookieValue = Cookies.get(`${prefix}-allow-cookie`);

    if(cookieValue && !!cookieValue === true) {
        return false;
    }

    let exclude = $body.getAttribute('data-cookie-exclude');

    if(exclude && exclude === window.location.pathname) {
        return false;
    }


    let allowText = $body.getAttribute('data-cookie-allow-btn') || 'Принять';
    let moreText = $body.getAttribute('data-cookie-more-btn') || 'Подробнее';
    let moreLink = $body.getAttribute('data-cookie-more-link');
    let mainText = $body.getAttribute('data-cookie-text') || `Продолжая работу с ${window.location.host}, вы подтверждаете использование сайтом cookies вашего браузера с целью улучшить предложения и сервис на основе ваших предпочтений и интересов`;

    let rnd = Date.now();
    let allowId = `${prefix}-cookie-allow-${rnd}`;
    let moreId = `${prefix}-cookie-more-${rnd}`;

    let $substrate = document.createElement('div');
    $substrate.setAttribute('class', `${prefix}-cookie-substrate`);

    let $mainInfo = document.createElement('div');
    $mainInfo.setAttribute('class', `${prefix}-cookie`);
    $mainInfo.innerHTML =`<div class="${prefix}-cookie__inner">
      <div class="${prefix}-cookie__main-text">${mainText}</div>
      <div class="${prefix}-cookie__btns-container">
        <a href="${moreLink}" class="${prefix}-cookie__btn ${prefix}-cookie__btn--more" id="${moreId}">${moreText}</a>
        <a href="javascript:void(0)" class="${prefix}-cookie__btn ${prefix}-cookie__btn--allow" id="${allowId}">${allowText}</a>
      </div>
    </div>`;

    $body.insertBefore($substrate, $body.firstChild);
    $body.insertBefore($mainInfo, $body.firstChild);

    setTimeout(function() {
        if ($body.classList) {
            $body.classList.add(`${prefix}-cookie-inited`);
        } else {
            $body.className += ' ' + `${prefix}-cookie-inited`;
        }
    },0);

    document.getElementById(allowId).addEventListener('click', function(e) {
        e.preventDefault();
        Cookies.set(`${prefix}-allow-cookie`, true, {expires: 365});
        $mainInfo.parentNode.removeChild($mainInfo);
        $substrate.parentNode.removeChild($substrate);
    });

    if(!moreLink) {
        let moreBtn = document.getElementById(moreId);
        moreBtn.parentNode.removeChild(moreBtn)
    }
})();