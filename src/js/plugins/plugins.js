// Avoid `console` errors in browsers that lack a console.
(function () {
	var method;
	var noop = function () {
	};
	var methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeStamp', 'trace', 'warn'
	];
	var length = methods.length;
	var console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}
}());

// dev var
window.dev = true;
// d function
function d() {
	if (dev) {
		console.log.apply(console, arguments);
	}
}

// m5gallery
;(function($){
	var options = {
		items: 6,
		slideDuration: 300
	};

	$.fn.m5gallery = function(userOptions) {
		if(this.length == 0) return this;

		if(this.length > 1){
			this.each(function(){$(this).m5gallery(userOptions)});
			return this;
		}

		$.extend(options, userOptions);
		var el = this,
			items = $('> li', this),
			gal = {};

		el.addClass('m5-gallery');
		gal.items = items;

		items.find('a').click(function(e) {
			e.preventDefault();
			if($(this).parent().hasClass('active')) return false;
			items.removeClass('active');
			$(this).parent().addClass('active');
			changeImg($(this).attr('href'));
		});

		var init = function() {
			el.wrap('<div class="m5-gallery-wrapper"></div>').before('<div class="m5-gallery-main-view"></div>');

			gal.mainView = el.siblings('.m5-gallery-main-view');
			gal.mainView.append('<div class="m5-gallery-loader"></div>');

			el.wrap('<div class="m5-gallery-playlist"></div>');
			el.wrap('<div class="m5-gallery-playlist-wrapper"></div>');


			gal.playlist = el.parents('.m5-gallery-playlist');
			gal.playlist.height(el.outerHeight());
			gal.itemWidth = items.eq(0).outerWidth(true);

			gal.loader = gal.mainView.find('.m5-gallery-loader');



			if(gal.playlist.width() < gal.itemWidth * options.items) {
				options.items--;
				while(--options.items) {
					if(gal.playlist.width() < gal.itemWidth * options.items) {
						continue;
					} else {
						break;
					}
				}
				if (options.items == 0 || options.items == 1) options.items = 2;
			}

			el.parent().width(gal.itemWidth * options.items);

			if(el.width() > el.parent().width()) {
				createControls();
				gal.haveControls = true;
			} else {
				gal.haveControls = false;
			}

			gal.leftIndex = 0;
			gal.rightIndex = options.items - 1;

			items.eq(0).find('a').click();
		};

		var changeImg = function(src) {
			gal.loader.show();
			gal.mainView.find('img').css({'opacity': '0.5'});
			var img = new Image();

			img.onload = function () {
				gal.loader.hide();
				gal.mainView.find('img').remove();
				$(img).appendTo(gal.mainView).hide().fadeIn();
			};
			img.src = src;

			if(gal.haveControls) {
				gal.leftControl.removeClass('edge');
				gal.rightControl.removeClass('edge');
				if(activeItem().index() == 0) gal.leftControl.addClass('edge');
				if(activeItem().index() == items.length - 1) gal.rightControl.addClass('edge');
			}
			slideToActive();
		};

		var createControls = function() {
			gal.playlist.addClass('with-controls');
			gal.leftControl = $('<a href="javascript:void(0)" class="control left"></a>').appendTo(gal.playlist).on('click', prev);
			gal.rightControl = $('<a href="javascript:void(0)" class="control right"></a>').appendTo(gal.playlist).on('click', next);
		};

		var prev = function() {
			if(activeItem().index() == 0) return false;
			activeItem().prev().find('a').click();
		};

		var next = function() {
			if(activeItem().index() == items.length - 1) return false;
			activeItem().next().find('a').click();
		};

		var slideToActive = function() {
			var active = activeItem(), i = active.index(), slide = false, dist = 0;
//			if(active.index() == 0 || active.index() == items.length - 1) return false;
			if(i == gal.rightIndex || (i - 1) == gal.rightIndex){

				dist = -(active.position().left);

				if((items.length - i) < options.items) {
					dist += (options.items - (items.length - i))*gal.itemWidth;
				}
				slide = true;

			} else if(i == gal.leftIndex || (i + 1) == gal.leftIndex){

				dist = -(active.position().left - (gal.itemWidth * (options.items - 1)));

				if((i + 1) < options.items) {
					dist -= (options.items - (i + 1))*gal.itemWidth;
				}
				slide = true;
			}

			if(slide) {
				gal.leftIndex = Math.abs(dist/gal.itemWidth);
				gal.rightIndex = Math.abs(dist/gal.itemWidth) + options.items - 1;
				el.animate({'left': dist}, options.slideDuration);
			}
		};

		var setItems = function() {
			if(gal.playlist.width() < gal.itemWidth * options.items) {
				options.items--;
				while(--options.items) {
					if(gal.playlist.width() < gal.itemWidth * options.items) {
						continue;
					} else {
						break;
					}
				}
				if (options.items == 0 || options.items == 1) options.items = 2;
			}

			el.parent().width(gal.itemWidth * options.items);
		};

		var activeItem = function() {
			return el.find('> .active');
		};

		// public methods

		el.m5Reload = function(newOptions) {
			$.extend(options, newOptions);
			setItems();
			gal.leftIndex = 0;
			gal.rightIndex = options.items - 1;

			items.eq(0).find('a').click();
		};

		el.m5Next = next;

		el.m5Prev = prev;

		el.m5Goto = function(item) {
			gal.items.eq(item-1).find('a').click();
		};


		init();


		return this;
	};

	$('.m5-gallery').each(function() {
		var opt = $(this).data('m5-gallery');
		$(this).m5gallery(opt);
	});

})(jQuery);


// twDatePicker
(function ($) {
	function TwDatePicker(element, options) {
		var d = new Date(),
			today = new Date(),
			currentMonth = new Date(d.getFullYear(), d.getMonth(), 1),
			monthNamesRu = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
			monthNamesRuRes = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
			monthNamesEn = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			dayNamesRu = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
			dayNamesEn = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			monthNames,
			dayNames,
			calendar = '',
			selectStyle = '',
			self = this,
			selectDate = today,
			formattedDate;

		if (options.lang == 'ru') {
			monthNames = monthNamesRu;
			dayNames = dayNamesRu;
		} else {
			monthNames = monthNamesEn;
			dayNames = dayNamesEn;
		}

		this.elem = element;

		this.render = function () {
			this.elem.find('.tw-datepicker-year').html(currentMonth.getFullYear());
			this.elem.find('.tw-datepicker-controls span').html(monthNames[currentMonth.getMonth()]);
			this.elem.find('.tw-datepicker-calendar').html(createTable());

			this.elem.find('.tw-datepicker-calendar').on('click', 'a', setSelectDate);
			if (options.button == true) {
				this.elem.find('.tw-datepicker-button').on('click', function () {
					$('body').trigger('click');
				});
			}
			calendar = '';
		}

		this.next = function () {
			d.setDate(1);
			d.setMonth(d.getMonth() + 1);
			currentMonth = new Date(d.getFullYear(), d.getMonth(), 1);
			this.render();
		}

		this.prev = function () {
			d.setDate(1);
			d.setMonth(d.getMonth() - 1);
			currentMonth = new Date(d.getFullYear(), d.getMonth(), 1);
			this.render();
		}

		this.elem.find('.next, .prev').click(function (e) {
			(e.target.className == 'next') ? self.next() : self.prev();
		});

		function setSelectDate(e) {
			e.preventDefault();
			e.stopPropagation();

			self.elem.find('.selected').removeClass('selected');

			$(this).addClass('selected');
			selectDate = new Date(d.getFullYear(), d.getMonth(), $(this).text());

			if (options.separator == null)
				formattedDate = selectDate;
			else
				formattedDate = (selectDate.getDate() + options.separator + monthNamesRuRes[parseInt(selectDate.getMonth())] + options.separator + selectDate.getFullYear()).toLowerCase();


			if ($(options.to).is('input'))
				$(options.to).val(formattedDate);
			else
				$('.tw-datepicker-selected-date').html(formattedDate);

			if (options.button == false) {
				$('body').trigger('click');
			}
		}

		function createTable() {
			calendar += '<table class="tw-datepicker-table"><tr>';
			for (var i = 0; i <= dayNames.length - 1; i++) {
				calendar += '<th>' + dayNames[i] + '</th>';
			}
			calendar += '</tr><tr>';

			for (var i = 0; i < getDay(currentMonth); i++) {
				var lastMonth = new Date(d.getFullYear(), d.getMonth(), -getDay(currentMonth) + i + 1);
				calendar += '<td class="tw-disabled-date">' + lastMonth.getDate() + '</td>';
			}

			while (d.getMonth() == currentMonth.getMonth()) {
				if (today.getFullYear() == currentMonth.getFullYear() && today.getMonth() == currentMonth.getMonth() && today.getDate() == currentMonth.getDate())
					selectStyle = 'today';
				else if (selectDate.getFullYear() == currentMonth.getFullYear() && selectDate.getMonth() == currentMonth.getMonth() && selectDate.getDate() == currentMonth.getDate())
					selectStyle = 'selected';
				else
					selectStyle = '';
				calendar += '<td><a href="javascript: void(0)" class="' + selectStyle + '">' + currentMonth.getDate() + '</a></td>';
				currentMonth.setDate(currentMonth.getDate() + 1);
				if (getDay(currentMonth) == 0) calendar += '</tr><tr>';
			}

			if (getDay(currentMonth) != 0) {
				for (var i = getDay(currentMonth); i <= 6; i++) {
					var nextMonth = new Date(d.getFullYear(), d.getMonth() + 1, -getDay(currentMonth) + i + 1);
					calendar += '<td class="tw-disabled-date">' + nextMonth.getDate() + '</td>';
				}
			}

			calendar += '</tr></table>';

			return calendar;
		}

		function getDay(fromDate) {
			if (options.lang == 'ru') {
				var day = fromDate.getDay();
				if (day == 0) day = 7;
				return day - 1;
			} else {
				return fromDate.getDay();
			}
		}
	}

	$.fn.twDatePicker = function (options) {
		if (this.length == 0) return this;

		if (this.length > 1) {
			this.each(function () {
				$(this).twDatePicker(options)
			});
			return this;
		}

		var posFix, makeup, settings, toAppend, button;
		var defaultOptions = {
			'separator': null,
			'to': null,
			'lang': 'ru',
			'button': false
		};

		if ($(this).is('input')) {
			$(this).wrap('<div class="tw-datepicker-wrap"></div>');
			toAppend = $(this).parent('.tw-datepicker-wrap');
		} else {
			toAppend = $(this);
		}

		posFix = $(this).css('position');
		if (posFix != 'fixed' || posFix != 'absolute' || posFix != 'relative') $(this).css('position', 'relative');

		settings = !!options ? $.extend({}, defaultOptions, options) : defaultOptions;


		if (settings.to != null && $(settings.to).length) {
			settings.to = $(settings.to)[0];
		} else if ($('[data-tw-datepicker]').length) {
			settings.to = $('[data-tw-datepicker]')[0];
		} else {
			settings.to = $(this);
		}

		if (!$(settings.to).is('input'))
			$(settings.to).append('<div class="tw-datepicker-selected-date"></div>');

		(settings.button == false) ? button = '' : button = '<div class="tw-datepicker-button-block"><a href="javascript:void(0)" class="tw-datepicker-button">Готово</a></div>';
		makeup = '<div class="tw-datepicker"><div class="tw-datepicker-year"></div><div class="tw-datepicker-controls"><a class="prev" href="javascript:void(0)"></a><a class="next" href="javascript:void(0)"></a><span></span></div><div class="tw-datepicker-calendar"></div>' + button + '</div>';
		toAppend.append(makeup);


		$('.tw-datepicker').css({'top': $(this).outerHeight(), 'left': '50%'}).on('click', function (e) {
			e.stopPropagation();
		});

		$(this).click(function (e) {
			e.stopPropagation();
			var datepicker = $('.tw-datepicker', toAppend);
			datepicker.show().css('margin-left', -(datepicker.outerWidth() / 2));

			$('body').one('click', function () {
				datepicker.hide();
			})
		});

		var twDatePicker = new TwDatePicker(toAppend, settings);
		twDatePicker.render();
		// console.log(twDatePicker);
	}
})(jQuery);


// price behavior
$(function () {
	$('[data-behavior=price]').each(function () {
		$(this).html($(this).html().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1&nbsp;'));
	});
});


// equal height
function equalHeight() {
	var currentTallest = 0,
		currentRowStart = 0,
		currentDiv,
		rowDivs = new Array(),
		$el,
		topPosition = 0;
	$('[data-equal]').height('auto').each(function () {

		$el = $(this);
		topPosition = $el.position().top;

		if (currentRowStart != topPosition) {

			// we just came to a new row.  Set all the heights on the completed row
			for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}

			// set the variables for the new row
			rowDivs.length = 0; // empty the array
			currentRowStart = topPosition;
			currentTallest = $el.height();
			rowDivs.push($el);

		} else {

			// another div on the current row.  Add it to the list and check if it's taller
			rowDivs.push($el);
			currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);

		}

		// do the last row
		for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
			rowDivs[currentDiv].height(currentTallest);
		}

	});
}

equalHeight();
$(window).on('resize.equalHeight', equalHeight);



$(function() {
	$('[data-tw-toggle]').on('click', function(e) {
		e.preventDefault();
		if($(this).hasClass('js-main-iblock-arrow')) {
			$('.c-main-iblock').not($(this).parents('.c-main-iblock')).removeClass('is-opened')
		}
		var toggleId = $(this).data('tw-toggle'),
			toggleTarget = $('[data-tw-toggle-target="' + toggleId + '"]'),
			toggleClass = $(this).data('tw-toggle-class'),
			toggleText = $(this).data('tw-toggle-text'),
			selfToggle = $(this).data('tw-toggle-self');
		if(toggleText) {
			var el, text;
			($(this).data('tw-toggle-text-all-links')) ? el = $('[data-tw-toggle="' + toggleId + '"]') : el = $(this);
			text = $(this).text();
			el.data('tw-toggle-text', text);
			el.text(toggleText);
		}
		if(selfToggle) {
			$(this).data('tw-toggle-self', $(this).attr('class'));
			$(this).attr('class', selfToggle)
		}
		toggleTarget.toggleClass(toggleClass);
	});

	$('[data-tw-toggle-js]').on('click', function(e) {
		e.preventDefault();
		var toggleTarget = $(eval($(this).data('tw-toggle-js'))),
			toggleClass = $(this).data('tw-toggle-class'),
			selfToggle = $(this).data('tw-toggle-self');
		if(selfToggle) $(this).toggleClass(selfToggle);
		toggleTarget.toggleClass(toggleClass);
	});
});

$(function() {
	$('[data-slick]').each(function() {
		$(this).slick();
	});
});